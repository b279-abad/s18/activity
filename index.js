// console.log("Hello World!");

function returnSumOf5and15(){
	return 5 + 15;
}

let sumOf5and15 = returnSumOf5and15();
console.log("Displayed sum of 5 and 15");
console.log(sumOf5and15);


function returnDifferenceOf20and5(){
	return 20 - 5;
}

let differenceOf20and5 = returnDifferenceOf20and5();
console.log("Displayed difference of 20 and 5");
console.log(differenceOf20and5);

function returnProductOf50and10(){
	return 50 * 10;
}

let productOf50and10 = returnProductOf50and10();
console.log("The product of 50 and 10:");
console.log(productOf50and10);

function returnQuotientOf50and10(){
	return 50 / 10;
}

let quotientOf50and10 = returnQuotientOf50and10();
console.log("The quotient of 50 and 10:");
console.log(quotientOf50and10);

function returnQuotientOf50and10(){
	return 50 / 10;
}

const radius = 15;
const pi = Math.PI;
const areaOfCircle = (radius, pi) => {
	return pi * radius * radius;
};
console.log("The result of getting the area of a circle with 15 radius: " + areaOfCircle(radius, pi));

const arr = [20, 40, 60, 80];
const average = arr.reduce((a, b) => a + b, 0) / arr.length;
console.log("The average of 20, 40, 60 and 80:");
console.log(average);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		addNum: typeof addNum !== 'undefined' ? addNum : null,
		subNum: typeof subNum !== 'undefined' ? subNum : null,
		multiplyNum: typeof multiplyNum !== 'undefined' ? multiplyNum : null,
		divideNum: typeof divideNum !== 'undefined' ? divideNum : null,
		getCircleArea: typeof getCircleArea !== 'undefined' ? getCircleArea : null,
		getAverage: typeof getAverage !== 'undefined' ? getAverage : null,
		checkIfPassed: typeof checkIfPassed !== 'undefined' ? checkIfPassed : null,

	}
} catch(err){

}
